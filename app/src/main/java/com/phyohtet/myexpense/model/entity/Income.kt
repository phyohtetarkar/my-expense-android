package com.phyohtet.myexpense.model.entity

import androidx.room.*

@Entity(foreignKeys = [
    ForeignKey(entity = Category::class, parentColumns = ["id"], childColumns = ["category_id"])
])
data class Income(
    @PrimaryKey
    var id: Long,
    var title: String,
    var amount: Double,
    var month: Int,
    var remark: String?,

    @ColumnInfo(name = "category_id")
    var categoryId: Long
) {
    @Ignore
    var category: Category? = null
}