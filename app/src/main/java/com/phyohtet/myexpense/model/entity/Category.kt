package com.phyohtet.myexpense.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(
    @PrimaryKey
    var id: Long,
    var name: String,
    var color: String,
    var type: Type
) {
    enum class Type { INCOME, EXPENSE }
}