package com.phyohtet.myexpense.model.entity

import androidx.room.*
import java.util.*

@Entity(foreignKeys = [
    ForeignKey(entity = Category::class, parentColumns = ["id"], childColumns = ["category_id"])
])
data class Expense(
    @PrimaryKey
    var id: Long,
    var title: String,
    var amount: Double,
    var date: Date,
    var remark: String?,

    @ColumnInfo(name = "category_id")
    var categoryId: Long
) {
    @Ignore
    var category: Category? = null
}